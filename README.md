# Progetto_SO

Progetto esame Sistemi Operativi, semafori DisastrOS, Davide Margottini  

Progetto di Sistemi Operativi sull'implementazione di semafori.

Sono stati aggiunti gli hendler per le syscall.
Sono sati modificati alcuni file per un funzionamento corretto.
Sono state implementate le funzioni disastrOS_semopen, disastrOS_semwait,
disastrOS_sempost, disastrOS_semclose.
Modificato il file di test "disastrOS_test.c" per testare l'effettivo funzionamento
dei semafori (implementati precedentemente), grazie al paradigma produttore/consumatore.



Davide Margottini 1762031

