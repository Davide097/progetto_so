#include <stdio.h>
#include <unistd.h>
#include <poll.h>

#include "disastrOS.h"
#include "disastrOS_constants.h"
#include "disastrOS_globals.h"
#include "disastrOS_semaphore.h"

#define ID_PRODUCER 0
#define ID_CONSUMER 1
#define ID_STAMPA 2

#define PRODUCERS_SEM 100
#define CONSUMERS_SEM 0

#define N_ART 7
#define N_MAX 15
// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Hello, I am the sleeper, and I sleep %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    disastrOS_printStatus();
  }
}

void childFunction(void* args){
  printf("[INFO]sono la funzione figlio N %d\n",disastrOS_getpid()-1);
  int type=0;
  int mode=0;
  int fd=disastrOS_openResource(disastrOS_getpid(),type,mode);
  int pid = disastrOS_getpid();
  
  printf("[INFO]Apro semafori\n");
  
  int  empty= disastrOS_semOpen(ID_PRODUCER, PRODUCERS_SEM);  
  int  full = disastrOS_semOpen(ID_CONSUMER, CONSUMERS_SEM);
  disastrOS_sleep(10); 
  
  Semaphore* semcons =  SemaphoreList_byId(&semaphores_list, ID_CONSUMER);
  
  for (int i=0; i<(pid+1); ++i){
    //printf("PID: %d, iterate %d\n", disastrOS_getpid(), i);
    disastrOS_sleep((20-disastrOS_getpid())*5);
    
    if (pid % 2 == 0 ){ //PRODUCER
            disastrOS_semWait(empty);
            printf("[INFO]Prodotto un articolo.\n");
            printf("[INFO]Ci sono %d articoli disponibili\n" , semcons->count+1);
            disastrOS_semPost(full);
    }
    if (pid % 2 != 0){ //COSUMATORE
            disastrOS_sleep(8);//li faccio partire poco dopo in modo che i produttori riempiano il "magazzino"
            disastrOS_semWait(full);
            printf("[INFO]Prelevato un aricolo.\n");
            printf("[INFO]Ci sono %d articoli disponibili.\n" , semcons->count);
            disastrOS_semPost(empty);
    }
  }
  
  printf("********************************************************\n");
  printf("[INFO] Chiudo i semafori,PID: %d\n", disastrOS_getpid());
  disastrOS_semClose(empty);
  disastrOS_semClose(full);
  printf("********************************************************\n");
  disastrOS_exit(disastrOS_getpid()+1);
}


void initFunction(void* args) {
  disastrOS_printStatus();
  printf("hello, I am init and I just started\n");
  disastrOS_spawn(sleeperFunction, 0);
  

  printf("I feel like to spawn 14 nice threads\n");
  int alive_children=0;
  for (int i=0; i<N_MAX; ++i) {
    int type=0;
    int mode=DSOS_CREATE;
    //printf("mode: %d\n", mode);
    //printf("opening resource (and creating if necessary)\n");
    int fd=disastrOS_openResource(i,type,mode);
    printf("fd=%d\n", fd);
    disastrOS_spawn(childFunction, 0);
    alive_children++;
  }

  disastrOS_printStatus();
  int retval;
  int pid;
  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
    //disastrOS_printStatus();
    //printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
	//   pid, retval, alive_children);
    --alive_children;
  }
  printf("shutdown!");
  disastrOS_shutdown();
}

int main(int argc, char** argv){
  char* logfilename=0;
  if (argc>1) {
    logfilename=argv[1];
  }
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  printf("the function pointer is: %p", childFunction);
  // spawn an init process
  printf("start\n");
  disastrOS_start(initFunction, 0, logfilename);
  return 0;
}
